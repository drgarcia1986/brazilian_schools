# -*- coding: utf-8 -*-
import asyncio
import json

import aiohttp
import muffin

from brazilian_schools import app


@app.register('/')
class Index(muffin.Handler):

    SCHOOL_URL_FMT = 'http://educacao.dadosabertosbr.com/api/escola/{}'
    SEARCH_SCHOOL_URL = (
        'http://educacao.dadosabertosbr.com/api/escolas/buscaavancada?'
        'situacaoFuncionamento=1&energiaInexistente=on&aguaInexistente=on&'
        'esgotoInexistente=on'
    )

    @asyncio.coroutine
    def _get_school_detail(self, school):
        url = self.SCHOOL_URL_FMT.format(school['cod'])
        resp = yield from aiohttp.request('GET', url)
        detail = yield from resp.json()
        school.update({'detail': detail})
        return school

    @asyncio.coroutine
    def _get_schools(self):
        resp = yield from aiohttp.request('GET', self.SEARCH_SCHOOL_URL)
        return (yield from resp.json())

    @asyncio.coroutine
    def _get_cached_result(self):
        data = yield from app.ps.redis.get('cached_result')
        if data:
            return json.loads(data)

    @asyncio.coroutine
    def _set_cached_result(self, data):
        serialized_data = json.dumps(data)
        yield from app.ps.redis.set('cached_result', serialized_data)

    def get(self, request):
        # Check cache first
        cached_result = yield from self._get_cached_result()
        if cached_result is not None:
            return app.ps.jinja2.render(
                'index.html',
                **cached_result
            )

        # Get school list
        schools_without_energy, school_list = yield from self._get_schools()

        # Create a task list
        school_detail_tasks = [
            self._get_school_detail(school) for school in school_list
        ]

        # Parse task list to a courotine task list (or asyncio wait)
        wait_detail = asyncio.wait(school_detail_tasks)
        school_list_with_details, _ = yield from wait_detail

        # Parse result (all results of task list is a future finalized)
        data = {
            'schools_without_energy': schools_without_energy,
            'school_list': [f.result() for f in school_list_with_details]
        }

        # update cache
        yield from self._set_cached_result(data)

        return app.ps.jinja2.render(
            'index.html',
            **data
        )
