PLUGINS = (
    'muffin_jinja2',
    'muffin_redis',
)

JINJA2_TEMPLATE_FOLDERS = 'brazilian_schools/templates'

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
