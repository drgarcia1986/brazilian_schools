import muffin


app = muffin.Application(
    'brazilian_schools',
    CONFIG='brazilian_schools.config.production'
)

from .views import *  # noqa
